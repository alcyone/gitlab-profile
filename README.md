# Alcyone

Alcyone instantiates computing environments from files committed to a Git repository

- Helm charts
    - Reana
    - Pathway Tools
    - m2m-jupyterhub
    - ~~JupyterHub~~
- Kustomizations
- Container image builds
    - Calrissian
- GitLab Workspaces
